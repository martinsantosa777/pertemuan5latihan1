package comebackisreal.com.pert6l1

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class myContact( var nama : String, var nomorHp : String) : Parcelable