package comebackisreal.com.pert6l1

import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        showDetail.setOnClickListener {
            showNotivy()
        }

    }

    private fun showNotivy() {
        val notfyDetailIntent = Intent(this@MainActivity, FormActivity::class.java)
        val myPendingIntent = TaskStackBuilder.create(this)
            .addParentStack(FormActivity::class.java)
            .addNextIntent(notfyDetailIntent)
            .getPendingIntent(110, PendingIntent.FLAG_UPDATE_CURRENT)

        val myNotfyManager = this.getSystemService(android.content.Context.NOTIFICATION_SERVICE) as NotificationManager
        val myBuilder = NotificationCompat.Builder(this)
            .setContentTitle("Show Add Form")
            .setContentText("Klik Me")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(myPendingIntent)
            .setAutoCancel(true)

        myNotfyManager.notify(1101,myBuilder.build())
    }

}
