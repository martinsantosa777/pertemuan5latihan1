package comebackisreal.com.pert6l1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_empty.*

class EmptyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_empty)

        if(myListContact.size!=0){
            val contactAdapter = myAdapterRecyView(myListContact)
            myRecView.apply {
                layoutManager = LinearLayoutManager(this@EmptyActivity)
                adapter = contactAdapter

            }
        }
    }
}
