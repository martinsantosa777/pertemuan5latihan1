package comebackisreal.com.pert6l1

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_form.*

var myListContact : ArrayList<myContact> = arrayListOf()
class FormActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)

        btnAddContact.setOnClickListener {
            if (txtNama.text.isNullOrEmpty() == true || txtNoHP.text.isNullOrEmpty() == true) {
                Toast.makeText(this, "Isian belum lengkap...", Toast.LENGTH_LONG).show()
            }
            else{
                var tmp : myContact = myContact(txtNama.text.toString(),txtNoHP.text.toString())
                myListContact.add(tmp)

                Toast.makeText(this, "Data Addes", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        var intent = Intent(this, EmptyActivity::class.java)
        startActivity(intent)
    }
}
